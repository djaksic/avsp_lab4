import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Scanner;

public class NodeRank {

    private int n;
    private double beta;
    private int[][] nodes;
    private int[][] queries;
    private int Q;
    private double[][] r;
    private int maxT = 0;

    private void readInput() {
        Scanner sc = new Scanner(System.in);
        String[] firstLineChunks = sc.nextLine().split("\\s+");
        n = Integer.parseInt(firstLineChunks[0]);
        beta = Double.parseDouble(firstLineChunks[1]);

        String[] lineChunks;
        nodes = new int[n][];
        for (int i = 0; i < n; i++) {
            lineChunks = sc.nextLine().split("\\s+");
            nodes[i] = new int[lineChunks.length];
            for (int j = 0; j < lineChunks.length; j++) {
                nodes[i][j] = Integer.parseInt(lineChunks[j]);
            }
        }
        Q = Integer.parseInt(sc.nextLine());
        queries = new int[Q][2];
        for (int i = 0; i < Q; i++) {
            lineChunks = sc.nextLine().split("\\s+");
            queries[i] = new int[lineChunks.length];
            queries[i][0] = Integer.parseInt(lineChunks[0]);
            queries[i][1] = Integer.parseInt(lineChunks[1]);
            if (queries[i][1] > maxT) {
                maxT = queries[i][1];
            }
        }
    }

    private void printInput() {
        StringBuilder sb = new StringBuilder();
        sb.append(n).append(" ").append(beta).append("\n");
        for (int i = 0; i < n; i++) {
            for (int j : nodes[i]) {
                sb.append(j).append(" ");
            }
            sb.append("\n");
        }
        sb.append(Q).append("\n");
        for (int i = 0; i < Q; i++) {
            for (int j : queries[i]) {
                sb.append(j).append(" ");
            }
            sb.append("\n");
        }
        System.out.println(sb);
    }



    private void init_r() {
        r = new double[maxT+1][n];
        Arrays.fill(r[0], 1./n);
        for (int i = 1; i <= maxT; i++) {
            Arrays.fill(r[i], my_round((1 - beta) / n));
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < nodes[j].length; k++) {
                    r[i][nodes[j][k]] += beta * r[i-1][j] / nodes[j].length;
                }
            }
        }
    }

    public static void main(String[] args) {
        NodeRank nr = new NodeRank();
        nr.readInput();
        nr.init_r();
        nr.evaluate();
    }

    private void evaluate() {
        NumberFormat formatter = new DecimalFormat("#0.0000000000");
        for (int i = 0; i < Q; i++) {
            System.out.println(formatter.format(r[queries[i][1]][queries[i][0]]));
        }
    }

    private double my_round(double x) {
        return Math.round(x * 1e10) / 1e10;
    }

}
