import java.util.*;

public class ClosestBlackNode {

    private int n, e;
    private int[] nodes;
    private Map<Integer, List<Integer>> edges = new HashMap<>();

    private void readInput() {
        Scanner sc = new Scanner(System.in);
        String[] firstLineChunks = sc.nextLine().split("\\s+");
        n = Integer.parseInt(firstLineChunks[0]);
        e = Integer.parseInt(firstLineChunks[1]);
        nodes = new int[n];

        for (int i = 0; i < n; i++) {
            nodes[i] = Integer.parseInt(sc.nextLine());
            edges.put(i, new ArrayList<>());
        }

        String[] lineChunks;
        int s, d;
        for (int i = 0; i < e; i++) {
            lineChunks = sc.nextLine().split("\\s+");
            s = Integer.parseInt(lineChunks[0]);
            d = Integer.parseInt(lineChunks[1]);
            edges.get(s).add(d);
            edges.get(d).add(s);
        }

        for (int i = 0; i < n; i++) {
            edges.get(i).sort(Integer::compareTo);
        }
    }

    private int[] bfs(int start) {
        boolean[] visited = new boolean[n];
        visited[start] = true;

        LinkedList<Integer> queue = new LinkedList<>();
        List<Integer> helpQueue = new LinkedList<>();
        List<Integer> blacks = new ArrayList<>();

        queue.add(start);
        int depth = 1;
        while (queue.size() != 0) {
            start = queue.poll();
            Iterator<Integer> i = edges.get(start).listIterator();
            while (i.hasNext()) {
                int next = i.next();
                if (nodes[next] == 1) {
                    blacks.add(next);
                }
                if (!visited[next]) {
                    visited[next] = true;
                    helpQueue.add(next);
                }
            }
            if (queue.size() == 0) {
                if (!blacks.isEmpty()) {
                    return new int[]{Collections.min(blacks), depth};
                }
                queue.addAll(helpQueue);
                helpQueue = new LinkedList<>();
                depth++;
            }
            if (depth > 10) {
                return new int[]{-1, -1};
            }
        }
        return new int[]{-1, -1};
    }

    private void findClosestBlackNode() {
        int[] ret;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            if (nodes[i] == 1) {
                ret = new int[]{i, 0};
            } else {
                ret = bfs(i);
            }
            sb.append(ret[0]).append(" ").append(ret[1]).append("\n");
        }
        sb = sb.deleteCharAt(sb.length()-1);
        System.out.println(sb);
    }

    private void printEdges() {
        for (int i = 0; i < n; i++) {
            System.out.println(edges.get(i));
        }
    }

    public static void main(String[] args) {
        ClosestBlackNode closestBlackNode = new ClosestBlackNode();
        closestBlackNode.readInput();
        closestBlackNode.findClosestBlackNode();
    }
}
